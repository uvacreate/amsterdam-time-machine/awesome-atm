# Awesome Amsterdam Time Machine [![Awesome](https://awesome.re/badge.svg)](https://awesome.re)

> A curated list of useful tools, resources and data to contribute to Amsterdam Time Machine related projects.

*What is an [awesome](https://github.com/sindresorhus/awesome) list?*

This list collects useful tools, resources and data that may provide you with a starting point for making your data fit in the Amsterdam Time Machine, or can help you in structuring your data by pointing to interesting examples and existing datasets. 
If you wish to contribute, send a PR or [get in touch](mailto:amsterdamtimemachine@uva.nl) with us! 

## Contents

- [Projects](#projects)
- [Datasets](#datasets)
- [Ontologies/Vocabularies](#ontologiesvocabularies)
- [Tools](#tools)
- [Crowdsourcing](#crowdsourcing)

## Projects

* [Golden Agents](https://www.goldenagents.org/) - Research infrastructure with data on creative industries in Amsterdam (1570-1750), in particular the producers and consumers of creative goods.

### 3D
* [3D Amsterdam](https://3d.amsterdam/) - 3D reconstruction of the entire city of Amsterdam.
* [Waterlooplein 3D](https://waterlooplein3d.nl/) - Open Source 3D reconstruction of the Waterlooplein area. 

### Geo
* [HISGIS Location points](https://hdl.handle.net/10622/FHJJYK) - Concordance for Amsterdam addresses in 1832, 1853, 1876, 1909, and 1943.

## Datasets

* [Adamlink](https://www.adamlink.nl/) - Reference data for Amsterdam collections, such as buildings, streets, neighbourhoods and persons. 
## Ontologies/Vocabularies

*Check out the [Awesome Humanities Ontologies](https://github.com/CLARIAH/awesome-humanities-ontologies) list*

* [Person Name Vocabulary (PNV)](https://w3id.org/pnv#) - An rdf vocabulary and data model for persons' names that allows for easy alignment of name elements between datasets. 
* [schema.org](https://schema.org/) - A straightforward vocabulary and data model for describing web content.
* [Reconstructions and Observations in Archival Resources (ROAR)](https://w3id.org/roar#) - A vocabulary and data model for archival resources.

## Tools

## Crowdsourcing

### Platforms
* [Het Volk](https://hetvolk.org/) - A crowdsourcing platform developed by [Hic Sunt Leones](http://www.hicsuntleones.nl/)
* [VeleHanden](https://velehanden.nl/) - A crowdsourcing platform developed by [Picturae](http://www.picturae.com/)

## Contribute

Contributions welcome! Read the [contribution guidelines](contributing.md) first.

## License

[![License: CC0-1.0](https://licensebuttons.net/l/zero/1.0/80x15.png)](http://creativecommons.org/publicdomain/zero/1.0/) CC0 1.0 Universal (CC0 1.0)
Public Domain Dedication